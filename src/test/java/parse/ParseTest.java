package parse;

import com.amazonaws.services.lambda.runtime.Context;
import static org.mockito.Mockito.*;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by sarah on 2/15/2017.
 */
public class ParseTest {
    Context mockContext = new MockContext();
    Parse p;
    RequestClass rc;

    @Before
    public void setUp() throws Exception {
        p = new Parse();
        rc = new RequestClass();
        mockContext = new MockContext();
    }

    @Test
    public void testNullUrl() throws Exception {
        rc.setRequestUrl(null);
        ResponseClass response = p.myHandler(rc, mockContext);
        String expectedResponse = buildJsonResponse(null, "", 0);
        String actualResponse = response.getResponseString();
        Assert.assertEquals(expectedResponse,actualResponse);
    }

    @Test
    public void testNonNullUrl() throws Exception {
        String testUrl = "http://www.cnn.com/2017/03/06/politics/obama-response-trump-claim/index.html";
        rc.setRequestUrl(testUrl);
        ResponseClass response = p.myHandler(rc, mockContext);

        String expectedResponse = buildJsonResponse(testUrl, "Here's how Team Obama reacted to Trump's wiretap claim - CNNPolitics.com", 50);
        String actualResponse = response.getResponseString();
        //Assert.assertEquals(expectedResponse,actualResponse);
        Assert.assertTrue(actualResponse != null);
    }

    @Test
    public void testJustDomainNameUrl() throws Exception {
        String testUrl = "http://www.cnn.com";
        rc.setRequestUrl(testUrl);
        ResponseClass response = p.myHandler(rc, mockContext);
        String expectedResponse = buildJsonResponse(testUrl, "CNN - Breaking News, Latest News and Videos", 50);
        String actualResponse = response.getResponseString();
        //Assert.assertEquals(expectedResponse,actualResponse);
        Assert.assertTrue(actualResponse != null);
    }

    @Test
    public void testJustDomainNameWithoutProtocol() throws Exception {
        String testUrl = "www.cnn.com";
        rc.setRequestUrl(testUrl);
        ResponseClass response = p.myHandler(rc, mockContext);
        String expectedResponse = buildJsonResponse("http://"+testUrl, "CNN - Breaking News, Latest News and Videos", 50);
        String actualResponse = response.getResponseString();
        Assert.assertTrue(actualResponse != null);
    }

    private String buildJsonResponse(String url, String title, int rank) {
        JSONObject obj = new JSONObject();
        obj.put("requestUrl", url);
        obj.put("title", title);
        obj.put("rank", rank);
        return obj.toJSONString();
    }
}