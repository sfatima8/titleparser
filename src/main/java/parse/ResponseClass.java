package parse;

public class ResponseClass {
    String responseString;
    public ResponseClass() {  
    	this.responseString = "";
    }
    public ResponseClass(String in_responseString) {
    	this.responseString = in_responseString;
    }
    public String getResponseString() {
    	return this.responseString;
    }
    public void setResponseString(String in_responseString) {
    	this.responseString = in_responseString;
    }
}
