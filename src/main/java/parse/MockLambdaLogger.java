package parse;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

/**
 * Created by sarah on 2/15/2017.
 */
public class MockLambdaLogger implements LambdaLogger {
    public void log(String s) {
        System.out.println(s);
    }
}
