package parse;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * verifies the rank of a URL based on items in the URL database
 * 
 * @author Chris
 *
 */
class UrlChecker {
    /**
     * gets the rank of the URL from the URLs database table
     * @return rank
     * @throws RankCalculationFailureException when the database access fails
     */
    static int getRank(RequestClass mRequest) throws RankCalculationFailureException {
    	 String tableName = "UrlRanks";
		 HashMap<String,AttributeValue> key_to_get = new HashMap<String,AttributeValue>();

		System.out.println("The url requested from the database: " +mRequest.requestUrl);

		// query the database for the requested URL
		key_to_get.put("UrlName", new AttributeValue(mRequest.requestUrl));

		 GetItemRequest request = new GetItemRequest()
				                       .withKey(key_to_get)
				                       .withTableName(tableName);

		int rank = 50;

		BasicAWSCredentials basic = new BasicAWSCredentials("AKIAINLTALKWM6B2TLEA", "3dusQmRj9LJDXWoM/IgQdiZIf11XlSkR1Y8bsmRy");
		AmazonDynamoDBClient ddb = new AmazonDynamoDBClient(basic);
		ddb.setEndpoint("dynamodb.us-west-2.amazonaws.com");
		try {
			System.out.println("Database Request Object: " + request);
			Map<String,AttributeValue> returned_item = ddb.getItem(request).getItem();
			if (returned_item != null) {
				System.out.println("Returned Items from the database: " + returned_item.size());
				Set<String> keys = returned_item.keySet();
				for (String key : keys) {
					if(key.equals("UrlRank")) {
						// get rank from database
						rank = Integer.parseInt(returned_item.get(key).getN());
						System.out.println("Rank from database is: " + rank);
					}
				}
			} else {
				System.out.println("Url not found in the database.");
			}
		} catch (AmazonServiceException e) {
			System.out.println(e.getErrorMessage());
			return rank;
		}
		
		return rank;
    }
}
