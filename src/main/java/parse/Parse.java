package parse;

import google.Search;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.json.simple.JSONObject;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import java.util.Set;

/**
 * Created by sarah on 2/13/2017.
 */
public class Parse {

    private String getTitle(String url) {
        String title = "";
        System.out.println("The url is: " + url);
        try {
            Document doc = Jsoup.connect(url).get();
            title = doc.title();
            System.out.println("The title is: " + title);
        } catch (Exception e) {
        }
        return title;
    }

    private Set<String> hasOtherGoogleResults(String title) {
        Set<String> result = null;
        try {
            result = new Search().getDataFromGoogle(title);
        } catch (Exception e) {
        }
        return result;
    }

    public ResponseClass myHandler(RequestClass requestClass, Context context) {
        String requestUrl = requestClass.requestUrl;
        final LambdaLogger logger = context.getLogger();
        logger.log("Received url : " + requestUrl + "\n");
        int rank = 0;
        String title = "";
        if (validateUrl(requestUrl)) {
            requestUrl = addProtocol(requestUrl);
            title = getTitle(requestUrl);
            Set<String> relativeUrls = hasOtherGoogleResults(title);
            if (relativeUrls != null) {
                System.out.println("The size is: " + relativeUrls.size());
                int validUrlCount = 0;
                for (String url : relativeUrls) {
                    requestClass.setRequestUrl(url);
                    System.out.println("Each url is: " + url);
                    try {
                        int currentUrlRank = UrlChecker.getRank(requestClass);
                        rank += currentUrlRank;
                        validUrlCount++;
                    } catch (RankCalculationFailureException e) {
                        logger.log("url check failed: " + e.getMessage());
                    }
                }
                if (validUrlCount > 0 && rank > 0) {
                    rank = rank / validUrlCount;
                }
            }
        }
        return buildResponse(requestUrl, title, rank);
    }

    private boolean validateUrl(String url) {
        boolean valid = false;
        if (url != null) {
            valid = true;
        }
        return valid;
    }

    private String addProtocol(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        return url;
    }

    private String buildJsonResponse(String url, String title, int rank) {
        JSONObject obj = new JSONObject();
        obj.put("requestUrl", url);
        obj.put("title", title);
        obj.put("rank", rank);
        return obj.toJSONString();
    }

    private ResponseClass buildResponse(String requestUrl, String title, int rank) {
        final ResponseClass response = new ResponseClass();
        String returnString = buildJsonResponse(requestUrl, title, rank);
        response.setResponseString(returnString);
        System.out.println(returnString);
        return response;
    }


}
